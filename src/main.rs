extern crate syscall;

use syscall::data::Packet;
use syscall::flag::CloneFlags;
use std::fs::File;
use std::io::{Read, Write};
use syscall::scheme::Scheme;



mod scheme;

fn main() {
    if unsafe { syscall::clone(CloneFlags::empty()).unwrap() } == 0 {
        let mut socket = File::create(":break").expect("breakerd: failed to create breaker scheme");
        let scheme = scheme::BreakerScheme::new();

        syscall::setrens(0, 0).expect("breakerd: failed to enter null namespace");

        loop {
            let mut packet = Packet::default();
            if socket.read(&mut packet).expect("breakerd: failed to read events from breaker scheme") == 0 {
                break;
            }

            scheme.handle(&mut packet);

            socket.write(&packet).expect("breakerd: failed to write responses to breaker scheme");
        }
    }

}
