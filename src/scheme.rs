use syscall::scheme::Scheme;

pub struct BreakerScheme {}

impl BreakerScheme {
    pub fn new() -> Self {
        return BreakerScheme {};
    }
}

impl Scheme for BreakerScheme {
    fn open(&self, path: &str, flags: usize, uid: u32, gid: u32) -> syscall::Result<usize> {
        return Ok(0);
    }

    fn read(&self, id: usize, buf: &mut [u8]) -> syscall::Result<usize> {
        let prefix_data = unsafe {
            let ptr = buf.as_ptr();
            let ptr = ptr.offset(-1);
            *ptr
        };

        buf[0] = prefix_data;

        return Ok(0);
    }

    fn close(&self, id: usize) -> syscall::Result<usize> {
        return Ok(0);
    }
}
